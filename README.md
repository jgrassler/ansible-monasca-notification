# monasca-notification
Installs the [monasca-notification](https://github.com/stackforge/monasca-notification) in a virtualenv.
Monasca Notification is part of the [Monasca](https://wiki.openstack.org/wiki/Monasca) project.

## Requirements
virtualenv must be installed.

- kafka_hosts - comma seperate list of kafka hosts
- mysql_host
- mysql_user
- mysql_password
- smtp_host
- smtp_port
- zookeeper_hosts - comma seperate list of zookeeper hosts

Optionally if needed
- pip_index_url: Index URL to use instead of the default for installing pip packages
- smtp_user
- smtp_password
- notification_enable_email: Set to false to disable email notifications
- notification_enable_webhook: Set to false to disable webhook notifications
- notification_enable_pagerduty: Set to false to disable pagerduty notifications
- notification_log_level: Log level for the Notification log, default to WARN
- mysql_ssl
  - This is a dictionary corresponding to the options in http://dev.mysql.com/doc/refman/5.0/en/mysql-ssl-set.html
  - For Example - {'ca':'/path/to/ca'}
- notification_run_mode: One of Deploy, Stop, Install, Configure or Start. The default is Deploy which will do Install, Configure, then Start.
- notification_statsd_host: Host of the statsd daemon, default is 'localhost'.
- notification_statsd_port: Port number of the statsd daemon, default is 8125.
- notification_install_user_group: Should user and group be installed in system, defaults to ```True```

## Example Playbook

    hosts: monasca
    become: yes
    roles:
      - {role: tkuhlman.monasca-notification,
         mysql_user: "{{notification_mysql_user}}",
         mysql_password: "{{notification_mysql_password}}",
         tags: [notification]}

## License
Apache

## Author Information
Tim Kuhlman
Monasca Team email monasca@lists.launchpad.net
